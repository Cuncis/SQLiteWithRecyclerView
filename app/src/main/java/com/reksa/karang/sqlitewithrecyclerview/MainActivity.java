package com.reksa.karang.sqlitewithrecyclerview;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText etInput;
    TextView tvCountNumber;
    int mAmount;
    GroceryDBHelper dbHelper;
    SQLiteDatabase db;
    GroceryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new GroceryDBHelper(this);
        db = dbHelper.getWritableDatabase();

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new GroceryAdapter(this, getAllItems());
        recyclerView.setAdapter(mAdapter);

        etInput = findViewById(R.id.et_input);
        tvCountNumber = findViewById(R.id.tv_number_count);

        Button btnDecrease = findViewById(R.id.btn_decrease);
        Button btnIncrease = findViewById(R.id.btn_increase);
        Button btnAdd = findViewById(R.id.btn_add);

        btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                decrease();
            }
        });

        btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increase();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });
    }

    private void add() {
        if (etInput.getText().toString().trim().length() == 0 || mAmount == 0) {
            return;
        }

        String name = etInput.getText().toString();
        ContentValues values = new ContentValues();
        values.put(GroceryContract.GroceryEntry.COL_NAME, name);
        values.put(GroceryContract.GroceryEntry.COL_AMOUNT, mAmount);

        db.insert(GroceryContract.GroceryEntry.TABLE_NAME, null, values);
        mAdapter.swapCursor(getAllItems());     //input recyclerview and always update list data

        etInput.getText().clear();
    }

    private void increase() {
        mAmount++;
        tvCountNumber.setText(String.valueOf(mAmount));
    }

    private void decrease() {
        if (mAmount > 0) {
            mAmount--;
            tvCountNumber.setText(String.valueOf(mAmount));
        }
    }

    private Cursor getAllItems() {
        return db.query(
                GroceryContract.GroceryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                GroceryContract.GroceryEntry.COL_TIMESTAMP + " DESC"    //new item always on top
        );
    }
}
