package com.reksa.karang.sqlitewithrecyclerview;

import android.provider.BaseColumns;

public class GroceryContract {

    public GroceryContract() {
    }

    public static final class GroceryEntry implements BaseColumns {
        public static final String TABLE_NAME = "groceryList";
        public static final String COL_NAME = "name";
        public static final String COL_AMOUNT = "amount";
        public static final String COL_TIMESTAMP = "timestamp";
    }
}
